<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class formController extends Controller
{
    public function __construct()
    {
        // here we have to provide the middleware class name not the middleware name
        $this->middleware('checkauth'); 

        // Here we need all the methods to pass the middleware otherwise we can use only helper function for specific methods
       
    }
    public function advanced()
    {
        return view('admin.pages.forms.advanced');
    }

    public function editors()
    {
        return view('admin.pages.forms.editors');
    }
    public function general2()
    {
        return view('admin.pages.forms.general');
    }
    public function validation()
    {
        return view('admin.pages.forms.validation');
    }
}
