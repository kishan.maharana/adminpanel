<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MailboxController extends Controller
{
	public function __construct()
    {
        // here we have to provide the middleware class name not the middleware name
        $this->middleware('checkauth'); 

        // Here we need all the methods to pass the middleware otherwise we can use only helper function for specific methods
       
    }
   public function compose()
   {
   	return view('admin.pages.mailbox.compose');
   }
   public function mailBox()
   {
   	return view('admin.pages.mailbox.mailbox');
   }
   public function readMail()
   {
   	return view('admin.pages.mailbox.readmail');
   }
}
