<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\Profile;
use DB;
use Auth;
use Hash;
class ApiController extends Controller
{


	public function allPost()
	{
        // getting all the record of posts table and user table where posts.user_id = users.id  API 1

		$data = DB::table('posts')
		->join('users', 'users.id', '=', 'posts.user_id')
		->select('users.*', 'posts.*')
		->get();
		return $data;
	}
	public function specificData($post_id)
	{
        //Getting a specific record using the post id API 2


		$data=DB::table('posts')
		->select('user_id')
		->where('id',$post_id)
		->get();
		$data=$data[0];
    	$data=$data->user_id; //Getting the user_id from posts table

    	$all = DB::table('posts')
    	->join('users', 'users.id', '=', 'posts.user_id')
    	->select('users.*', 'posts.*')
    	->where('users.id','=',$data)
    	->get();
        return response()->json($all); // getting a specific record from both users table and posts table
    }
    public function update($id)
    {
        // API for updating a specific record in posts table for specifi id API 4

    	Post::where("id",$id)
    	->update(array('posttitle' => 'updating this record'));
    	return 'updated';
    }
    public function delete($id)
    {
        // API for deleteting a specific record from posts table for a specific id API 3

    	DB::table('posts')
    	->join('users', 'users.id', '=', 'posts.user_id')
    	->where('posts.id','=',$id)
    	->delete();
    	return 'deleted';
    }
    public function userwithpost()
    {
        // API for getting all users data having post API 6

    	$data = DB::table('posts')
    	->join('users', 'users.id', '=', 'posts.user_id')
    	->select('users.*')
    	->get();
    	return response()->json($data);
    }
    public function databetweendates()
    {
        // API for fetching all the records from posts table between two perticular dates API 5

    	$alldata=DB::table('posts')
    	->whereBetween('created_at',['20-08-18','20-08-20'])
    	->get();
    	return response()->json($alldata);

    }
    public function loginApi($email,$password)
    {
    	//API for taking email and password as parameter and checking for login
    	if(Auth::attempt(['email'=>$email,'password'=>$password]))
		{
			return 'Logged in!';
		}
		else
		{
			return 'incorrect Email or Password';
		}
    }

    public function signupApi(Request $req)
    {
    	//API for creating an user 
    	$data = new User;
    	$data->name=$req->name;
    	$data->email=$req->email;
    	$data->password=bcrypt($req->password);
    	$data->save();
    	return "saved";
    }

}
