<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class adminController extends Controller
{
    public function dashboard()
    {
        return view('admin.homepage');
    }
    public function dashboard2()
    {
        return view('admin.homepage2');
    }
    public function dashboard3()
    {
        return view('admin.homepage3');
    }
    public function widget()
    {
        return view('admin.pages.widget');
    }
}
