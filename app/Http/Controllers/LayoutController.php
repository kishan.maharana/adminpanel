<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class layoutController extends Controller
{
    public function __construct()
    {
        // here we have to provide the middleware class name not the middleware name
        $this->middleware('checkauth'); 

        // Here we need all the methods to pass the middleware otherwise we can use only helper function for specific methods
       
    }
    public function topNav()
    {
        return view('admin.pages.layout.topnav');
    }
    public function topNavSidebar()
    {
        return view('admin.pages.layout.topnavsidebar');
    }
    public function boxed()
    {
        return view('admin.pages.layout.boxed');
    }
    public function fixedSidebar()
    {
        return view('admin.pages.layout.fixedsidebar');
    }
    public function fixedNavbar()
    {
        return view('admin.pages.layout.fixedtopnav');
    }
    public function fixedFooter()
    {
        return view('admin.pages.layout.fixedfooter');
    }
    public function collapsedSidebar()
    {
        return view('admin.pages.layout.collapsedsidebar');
    }
    public function calender()
    {
        return view('admin.pages.calender');
    }
    public function gallery()
    {
        return view('admin.pages.gallery');
    }
}
