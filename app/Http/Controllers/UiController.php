<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class uiController extends Controller
{
    public function __construct()
    {
        // here we have to provide the middleware class name not the middleware name
        $this->middleware('checkauth'); 

        // Here we need all the methods to pass the middleware otherwise we can use only helper function for specific methods
       
    }
     public function buttons()
    {
        return view('admin.pages.UI.buttons');
    }
    public function general()
    {
        return view('admin.pages.UI.general');
    }
    public function icons()
    {
        return view('admin.pages.UI.icons');
    }
    public function modals()
    {
        return view('admin.pages.UI.modals');
    }
    public function navbar()
    {
        return view('admin.pages.UI.navbar');
    }
    public function ribbons()
    {
        return view('admin.pages.UI.ribbons');
    }
    public function sliders()
    {
        return view('admin.pages.UI.sliders');
    }
    public function timeline()
    {
        return view('admin.pages.UI.timeline');
    }
}
