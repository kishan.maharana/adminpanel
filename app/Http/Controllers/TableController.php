<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
class tableController extends Controller
{
    public function __construct()
    {
        // here we have to provide the middleware class name not the middleware name
        $this->middleware('checkauth'); 

        // Here we need all the methods to pass the middleware otherwise we can use only helper function for specific methods
       
    }
    public function data()
    {
        $profile=DB::table('profiles')
                ->select('*')
                ->get();
    	return view('admin.pages.tables.data',compact('profile'));
    }
        public function jsgrid()
    {
        $post=DB::table('posts')
                ->select('*')
                ->get();
    	return view('admin.pages.tables.jsgrid',compact('post'));
    }
        public function simple()
    {
        $user=User::all();
    	return view('admin.pages.tables.simple',compact('user'));
    }

}
