@extends('layouts.app')
@section('content')

@if(count($errors)>0)

@foreach($errors->all() as $error)

<p class="alert alert-danger">{{$error}}</p>

@endforeach

@endif


<form action="{{url('/cregister')}}" method="post">
  {{csrf_field()}}
  <fieldset>
    <legend>Register</legend>
    <div class="form-group">
      <label for="exampleInputEmail1">Name</label>
      <input type="text" class="form-control" name="name" aria-describedby="emailHelp" placeholder="Enter Name">
     
    </div>
   
    <div class="form-group">
      <label for="exampleInputEmail1">Email address</label>
      <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
     
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Password</label>
      <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Password">
    </div>
     <div class="form-group">
      <label for="exampleInputPassword1"> Confirm Password</label>
      <input type="password" class="form-control" name="password_confirmation" placeholder="confirm Password">
    </div>
    <div class="form-group">
      
      <input type="submit" class="form-control btn btn-primary" >
    </div>
   
</form>


@endsection