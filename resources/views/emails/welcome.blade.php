@component('mail::message')
# Confirmation

Thank you registering with us . this is the sample mail for testing event listener

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
