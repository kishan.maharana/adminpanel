@extends('master')

  <!-- Content Wrapper. Contains page content -->

    @section('content')

 <div class="row">
      <<div class="col-12 text-center">
        <h3>Post Table</h3>
      </div>
    </div>
    <div class="row my-5">
      <<div class="col-1">
      </div>
    <div class="row-10 m-auto">
      <table class="table table-dark container-fluid">
    
      <thead>
        <tr>
          <th scope="col">Id</th>
          <th scope="col">Post Title</th>
          <th scope="col">Post Content</th>
    {{--       <th scope="col">Post Title</th>
          <th scope="col">Post Content</th> --}}
          
        </tr>
      </thead>
      <tbody>
    
        @foreach($post as $item)
        <tr>
          <th scope="row">{{$item->id}}</th>
          <td>{{$item->posttitle}}</td>
          <td>{{$item->postcontent}}</td>
        
                
        </tr>
        @endforeach
      </tbody>
    </table>
 
    </div>  
    <div class="col-1">
      </div>
    </div>
    



    @endsection

    @section('scripts')
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- jsGrid -->
<script src="../../plugins/jsgrid/demos/db.js"></script>
<script src="../../plugins/jsgrid/jsgrid.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#jsGrid1").jsGrid({
        height: "100%",
        width: "100%",
 
        sorting: true,
        paging: true,
 
        data: db.clients,
 
        fields: [
            { name: "Name", type: "text", width: 150 },
            { name: "Age", type: "number", width: 50 },
            { name: "Address", type: "text", width: 200 },
            { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
            { name: "Married", type: "checkbox", title: "Is Married" }
        ]
    });
  });
</script>
    @endsection