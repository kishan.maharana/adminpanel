@extends('master')

  <!-- Content Wrapper. Contains page content -->

    @section('content')

    <div class="row">
      <<div class="col-12 text-center">
        <h3>User Table One to One relationship with Profiles table and one to many relationship with Posts table</h3>
      </div>
    </div>
    <div class="row my-5">
      <<div class="col-1">
      </div>
    <div class="row-10 m-auto">
      <table class="table table-dark container-fluid">
    
      <thead>
        <tr>
          <th scope="col">id</th>
          <th scope="col">Name</th>
          <th scope="col">Email</th>
          <th scope="col">Address</th>
          <th scope="col">About</th>
          <th scope="col">Post Title</th>
          <th scope="col">Post Content</th>
    {{--       <th scope="col">Post Title</th>
          <th scope="col">Post Content</th> --}}
          
        </tr>
      </thead>
      <tbody>
    
        @foreach($user as $item)
        <tr>
          <th scope="row">{{$item->id}}</th>
          <td>{{$item->name}}</td>
          <td>{{$item->email}}</td>
                <td>{{$item->myProfile ? $item->myProfile->address : '' }}</td>
                <td>{{$item->myProfile ? $item->myProfile->about : '' }}</td>
               
                <td>

                  {{-- Loop for printing posts which is related to user with hasmany relationship --}}
                  <?php
                  $count=1;
                  $count2=1
                  ?>
                  @foreach($item->myPosts as $data)
                  <?php
                   
                    echo $count;
                    $count++;
                  ?>
                  {{$data ? $data->posttitle : '' }}<br>
                  @endforeach
                </td>

                
                <td>
                  @foreach($item->myPosts as $data)
                  <?php
                   
                    echo $count2;
                    $count2++;
                  ?>
                  {{$data ? $data->postcontent : '' }}<br>
                  @endforeach
                </td>
        </tr>
        @endforeach
      </tbody>
    </table>
 
    </div>	
    <div class="col-1">
      </div>
    </div>
    

    @endsection

    @section('scripts')

<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
    @endsection