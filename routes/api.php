<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Applying middleware to the API routes

// Creating API's for posts table

//APi for getting all record of posts table as well as users table
Route::get('/allpost','Api\ApiController@allPost');

//APi for getting a specific data from both users table and posts table
Route::get('/specificdata/{id}','Api\ApiController@specificData');

//API for updating a record in posts table
Route::get('/update/{id}','Api\ApiController@update');

// API for deleteting a specific record from posts table for a specific id
Route::get('/delete/{id}','Api\ApiController@delete');

// API for getting all users data having post
Route::get('/userwithpost','Api\ApiController@userwithpost');

//API for getting all data from post table between two perticular dates
Route::get('/databetweendates','Api\ApiController@databetweendates');

//API for taking email and password as a parameter
Route::get('/loginapi/{email}/{password}','Api\ApiController@loginApi');

//API for signing up an user
Route::post('/signupapi','Api\ApiController@signupApi');

